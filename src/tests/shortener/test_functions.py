from django.test import SimpleTestCase
from string import digits, ascii_letters
from shortener.functions import dec_to_base_n, get_identifier

class TestDecToBase(SimpleTestCase):

    def test_correct_value_default_keyword_args(self):
        self.assertEqual(dec_to_base_n(62**2), '100')

    def test_0_input(self):
        self.assertEqual(dec_to_base_n(0), '0')

    def test_correct_value_base_16(self):
        self.assertEqual(dec_to_base_n(45, base=13), '36')

    def test_return_type(self):
        self.assertTrue(isinstance(dec_to_base_n(34), str))

    def test_correct_value_custon_charset(self):
        self.assertEqual(dec_to_base_n(62**3, base=63, characters=digits + ascii_letters + '$'), 'Y2$' )

    def test_base_charset_mismatch(self):
        with self.assertRaises(IndexError):
            dec_to_base_n(62**3, base=63, characters=digits + ascii_letters)

    def test_wrong_input_type(self):
        with self.assertRaises(TypeError):
            dec_to_base_n('200')

    def test_negative_input(self):
        with self.assertRaises(ValueError):
            dec_to_base_n(-8)

    
class TestGetIdentifier(SimpleTestCase):

    def test_return_type(self):
        self.assertTrue(isinstance(get_identifier(), str))

    def test_default_length(self):
        self.assertEqual(len(get_identifier()), 7)

    def test_length(self):
        self.assertEqual(len(get_identifier(lenght=9)), 9)

    def test_negative_input(self):
        with self.assertRaises(ValueError):
            get_identifier(-8)
