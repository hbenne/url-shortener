from random import randint
from string import digits, ascii_letters


def dec_to_base_n(num, base=62, characters=digits + ascii_letters):
    """Converts a number to base n (default = 62).
    
    :param int num: Number to transform.
    :param int base: Selects the base in which the result is returned.
    :param str characters: Character set for displaying the result. 
    Length of the string must match the size of the selected base
    :return str result: The base n encodes number
    """
    result = ''
    if num < 0:
        raise ValueError('dec_to_base_n() not defined for negative values')
    if num == 0:
        return '0'
    while num != 0:
        reminder = num % base
        result = characters[reminder] + result
        num = int(num/base)
    return result


def get_identifier(lenght=7):
    """Calculates a random number between 1 and 62**:length:-1 and returns a base 62 string of length :length:.
    This string is used as identifier for urls.

    :param int length: Selects the base in which the result is returned.
    :return str identifier: Base 62 string.
    """
    if lenght < 0:
        raise ValueError('get_identifier() not defined for negative values')
    
    num = randint(1, 62**lenght - 1)
    identifier = dec_to_base_n(num)
    return identifier
    
    